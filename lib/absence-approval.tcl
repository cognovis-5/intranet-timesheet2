# ----------------------------------------------------------------------
# Inbox for "Business Objects"
# ----------------------------------------------------------------------

set view_name "absence_approval_inbox"
set order_by_clause ""
set relationship "assignment_group" 
set relationships {holding_user assignment_group none} 
set object_type ""
set subtype_id ""
set status_id ""

set bgcolor(0) " class=roweven "
set bgcolor(1) " class=rowodd "

set sql_date_format "YYYY-MM-DD"
set current_user_id [auth::get_user_id]
set return_url [im_url_with_query]
set view_id [db_string get_view_id "select view_id from im_views where view_name=:view_name"]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]

set date_format "YYYY-MM-DD"

set form_vars [ns_conn form]
if {"" == $form_vars} { set form_vars [ns_set create] }

# Order_by logic: Get form HTTP session or use default
if {"" == $order_by_clause} {
    set order_by [ns_set get $form_vars "wf_inbox_order_by"]
    set order_by_clause [db_string order_by "
		select	order_by_clause
		from	im_view_columns
		where	view_id = :view_id and
			column_name = :order_by
	" -default ""]
}

# Calculate the current_url without "wf_inbox_order_by" variable
set current_url "[ns_conn url]?"
ns_set delkey $form_vars wf_inbox_order_by
set form_vars_size [ns_set size $form_vars]
for { set i 0 } { $i < $form_vars_size } { incr i } {
    set key [ns_set key $form_vars $i]
    if {"" == $key} { continue }
    
    # Security check for cross site scripting
    if {![regexp {^[a-zA-Z0-9_\-]*$} $key]} {
		im_security_alert \
		    -location im_workflow_home_inbox_component \
		    -message "Invalid URL var characters" \
		    -value [ns_quotehtml $key]
		# Quote the harmful keys
		regsub -all {[^a-zA-Z0-9_\-]} $key "_" key
    }
    
    set value [ns_set get $form_vars $key]
    append current_url "$key=[ns_urlencode $value]"
    ns_log Notice "im_workflow_home_inbox_component: i=$i, key=$key, value=$value"
    if { $i < [expr $form_vars_size-1] } { append url_vars "&" }
}

if {"" == $order_by_clause} {
    set order_by_clause [parameter::get_from_package_key -package_key "intranet-workflow" -parameter "HomeInboxOrderByClause" -default "creation_date"]
}

# Let Admins see everything
if {[im_is_user_site_wide_or_intranet_admin $current_user_id]} { set relationship "none" }

    # Set relationships based on a single variable
case $relationship {
    holding_user { set relationships {my_object holding_user}}
    my_object { set relationships {my_object holding_user}}
    specific_assignment { set relationships {my_object holding_user specific_assigment}}
    assignment_group { set relationships {my_object holding_user specific_assigment assignment_group}}
    object_owner { set relationships {my_object holding_user specific_assigment assignment_group object_owner}}
    object_write { set relationships {my_object holding_user specific_assigment assignment_group object_owner object_write}}
    object_read { set relationships {my_object holding_user specific_assigment assignment_group object_owner object_write object_read}}
    none { set relationships {my_object holding_user specific_assigment assignment_group object_owner object_write object_read none}}
}

# ---------------------------------------------------------------
# Columns to show

set column_sql "
	select	column_id,
		column_name,
		column_render_tcl,
		visible_for,
		(order_by_clause is not null) as order_by_clause_exists_p
	from	im_view_columns
	where	view_id = :view_id
	order by sort_order, column_id
    "

set column_vars [list]
set colspan 1
set table_header_html "<tr class=\"list-header\">\n"

db_foreach column_list_sql $column_sql {
    if {"" == $visible_for || [eval $visible_for]} {
		lappend column_vars "$column_render_tcl"
		regsub -all " " $column_name "_" col_txt
		set col_txt [lang::message::lookup "" intranet-workflow.$col_txt $column_name]
		set col_url [export_vars -base $current_url {{wf_inbox_order_by $column_name}}]
		set admin_link "<a href=[export_vars -base "/intranet/admin/views/new-column" {return_url column_id {form_mode edit}}] target=\"_blank\">[im_gif wrench]</a>"
		if {!$user_is_admin_p} { set admin_link "" }
		if {"f" == $order_by_clause_exists_p} {
		    append table_header_html "<th class=\"list\">$col_txt$admin_link</td>\n"
		} else {
		    append table_header_html "<th class=\"list\"><a href=\"$col_url\">$col_txt</a>$admin_link</td>\n"
		}
		incr colspan
    }
}

append table_header_html "</tr>\n"


# ---------------------------------------------------------------
# SQL Query

# Get the list of all "open" (=enabled or started) tasks with their assigned users
set tasks_sql "
	select
		o.object_id,
		o.creation_user as creation_user,
        a.owner_id,
		o.creation_date,
		im_name_from_user_id(o.creation_user) as owner_name,
		acs_object__name(o.object_id) as object_name,
		im_biz_object__get_type_id(o.object_id) as type_id,
		im_biz_object__get_status_id(o.object_id) as status_id,
		tr.transition_name,
		t.holding_user,
		t.task_id,
                to_char(a.start_date, :date_format) as start_date_pretty,
                to_char(a.end_date, :date_format) as end_date_pretty,
                a.description
	from
		acs_objects o,
		wf_cases ca left outer join im_user_absences a on a.absence_id = ca.object_id,
		wf_transitions tr,
		wf_tasks t,
        wf_task_assignments wta
	where
        wta.task_id = t.task_id
        and (wta.party_id = :user_id or o.creation_user = :user_id or wta.party_id in (select group_id from group_member_map where member_id = :user_id))
		and o.object_id = ca.object_id
		and ca.case_id = t.case_id
		and t.state in ('active','enabled', 'started')
		and t.transition_key = tr.transition_key
		and t.workflow_key = tr.workflow_key
        and (t.workflow_key in (select distinct aux_string1 from im_categories where category_type = 'Intranet Absence Type' and aux_string1 is not null) or
             t.workflow_key = 'vacation_storno_wf')
        and a.absence_status_id not in (16002,16005)
    "

if {"" != $order_by_clause} {
    append tasks_sql "\torder by $order_by_clause"
}

# ---------------------------------------------------------------
# Store the conf_object_id -> assigned_user relationship in a Hash array
set tasks_assignment_sql "
    	select
		t.*,
		m.member_id as assigned_user_id
	from
		($tasks_sql) t
		LEFT OUTER JOIN (
			select distinct
				m.member_id,
				ta.task_id
			from	wf_task_assignments ta,
				party_approved_member_map m
			where	m.party_id = ta.party_id
		) m ON t.task_id = m.task_id
    "

# ---------------------------------------------------------------
# Format the Result Data

set ctr 0
set table_body_html ""
set object_ids [list]

db_foreach tasks $tasks_sql {

    lappend object_ids $object_id
    set rel "assignment_group" 
    
    if {[lsearch $relationships $rel] == -1} { continue }
        
    regsub -all "#" $transition_name "" transition_key
    if {$transition_name ne $transition_key} {
		set next_action_l10n [lang::message::lookup "" $transition_key]
    } else {
		# L10ned version of next action
		regsub -all " " $transition_name "_" next_action_key
		set next_action_l10n [lang::message::lookup "" intranet-workflow.$next_action_key $transition_name]
    }
    set object_subtype [im_category_from_id $type_id]
    set status [im_category_from_id $status_id]
    set object_url "[export_vars -base "/intranet-timesheet2/absences/new" {{form_mode display} {absence_id $object_id}}]"
    set owner_url [export_vars -base "/intranet/users/view" {return_url {user_id $owner_id}}]

    set approve_url [export_vars -base "/[im_workflow_url]/task" -url {{attributes.review_reject_p t} {action.finish "Task done"} task_id return_url}]
    set deny_url [export_vars -base "/[im_workflow_url]/task" -url {{attributes.review_reject_p f} {action.finish "Task done"} task_id return_url}]
    set workflow_url [export_vars -base "/[im_workflow_url]/task" -url {task_id return_url}]

    # if this is the creator viewing it, prevent him from approving it
    # himself
    if {$owner_id == $user_id} {
		set approve_url [export_vars -base "/[im_workflow_url]/task" {return_url task_id}]
		set next_action_l10n "View"
		set deny_url ""
    }


    
    # Don't show the "Action" link if the object is mine...
    if {"my_object" == $rel} {
		set action_link $next_action_l10n
    } 
    
    set action_link "asdf"
    
    # L10ned version of the relationship of the user to the object
    set relationship_l10n [lang::message::lookup "" intranet-workflow.$rel $rel]
    
    set row_html "<tr$bgcolor([expr $ctr % 2])>\n"
    foreach column_var $column_vars {
		append row_html "\t<td valign=top>"
		set cmd "append row_html $column_var"
		eval "$cmd"
		append row_html "</td>\n"
    }
    append row_html "</tr>\n"
    append table_body_html $row_html
    incr ctr
}

#---------------------------------------------------------------
# Check for disziplinarischer Vertreter
#---------------------------------------------------------------

# get the employees for whom I am currently the manager_vacation_replacement_id
set manager_vacation_replacement_ids [db_list supervisors "select owner_id from im_user_absences where manager_vacation_replacement_id = :current_user_id and start_date::date <= now()::date and end_date::date>=now()::date and absence_status_id = [im_user_absence_status_active]"]

if {[llength $manager_vacation_replacement_ids] >0 && [lindex $manager_vacation_replacement_ids 0] ne "" } {

    set manager_replacement_tasks_sql "
	select
		o.object_id,
		o.creation_user as creation_user,
        a.owner_id,
		wta.party_id,
		o.creation_date,
		im_name_from_user_id(o.creation_user) as owner_name,
		acs_object__name(o.object_id) as object_name,
		im_biz_object__get_type_id(o.object_id) as type_id,
		im_biz_object__get_status_id(o.object_id) as status_id,
		tr.transition_name,
		t.holding_user,
		t.task_id,
                to_char(a.start_date, :date_format) as start_date_pretty,
                to_char(a.end_date, :date_format) as end_date_pretty,
                a.description
	from
		acs_objects o,
		wf_cases ca left outer join im_user_absences a on a.absence_id = ca.object_id,
		wf_transitions tr,
		wf_tasks t,
        wf_task_assignments wta
	where
        wta.task_id = t.task_id
        and (wta.party_id in ([template::util::tcl_to_sql_list $manager_vacation_replacement_ids]) or o.creation_user in ([template::util::tcl_to_sql_list $manager_vacation_replacement_ids]) or wta.party_id in (select group_id from group_member_map where member_id in ([template::util::tcl_to_sql_list $manager_vacation_replacement_ids])))
		and o.object_id = ca.object_id
		and ca.case_id = t.case_id
		and t.state in ('active','enabled', 'started')
		and t.transition_key = tr.transition_key
		and t.workflow_key = tr.workflow_key
        and (t.workflow_key in (select distinct aux_string1 from im_categories where category_type = 'Intranet Absence Type' and aux_string1 is not null) or
             t.workflow_key = 'vacation_storno_wf')
        and a.absence_status_id not in (16002,16005)"
    
    if {[llength $object_ids] >0 } {
	append manager_replacement_tasks_sql "	and o.object_id not in ([template::util::tcl_to_sql_list $object_ids])"
    }
    
    # Now append the workflows with an "assign me" label
    db_foreach manager_sql $manager_replacement_tasks_sql {
		set status [im_category_from_id $status_id]
		set object_url "[export_vars -base "/intranet-timesheet2/absences/new" {{form_mode display} {absence_id $object_id}}]"
		set owner_url [export_vars -base "/intranet/users/view" {return_url {user_id $owner_id}}]
		set workflow_url [export_vars -base "/intranet-workflow/assign" {task_id {return_url [util_get_current_url]}}]
		
		# Set the supervisor name
		set supervisor_name [im_name_from_user_id $party_id]
		set next_action_l10n [lang::message::lookup "" intranet-workflow.Assign_yourself_instead "Assign to yourself instead of %supervisor_name%"]
		set deny_url ""
		set approve_url ""
	    
		set row_html "<tr$bgcolor([expr $ctr % 2])>\n"
		foreach column_var $column_vars {
		    append row_html "\t<td valign=top>"
		    set cmd "append row_html $column_var"
		    eval "$cmd"
		    append row_html "</td>\n"
		}
		append row_html "</tr>\n"
		append table_body_html $row_html
		incr ctr
    }
}


#---------------------------------------------------------------
# Check for supervisors manager
#---------------------------------------------------------------

# First check if the supervisor is on vacation
set supervisor_ids [db_list supervisors "select employee_id from im_employees e, im_user_absences u where supervisor_id = :current_user_id and employee_id in (select distinct supervisor_id from im_employees) and e.employee_id = u.owner_id and u.start_date::date <=now()::date and u.end_date::date >= now()::date and absence_status_id = [im_user_absence_status_active]"]

# Then check their replacement 
# and filter if the replacement is not on vacation
set missing_supervisor_ids [list]
foreach supervisor_id $supervisor_ids {
	set replacement_id [db_list replacement_id "select manager_vacation_replacement_id from im_user_absences where owner_id = :supervisor_id and start_date::date <=now()::date and end_date::date >= now()::date"]
	if { [db_string absent "select 1 from im_user_absences where owner_id = :replacement_id and start_date::date <=now()::date and end_date::date >= now()::date and absence_status_id = [im_user_absence_status_active]" -default 0]} {
		ns_log Notice "replace... $replacement_id"
		lappend missing_supervisor_ids $supervisor_id
	}
}

set supervisor_ids $missing_supervisor_ids

if {[llength $supervisor_ids] >0 && [lindex $supervisor_ids 0] ne "" } {
	set manager_tasks_sql "
	select
		o.object_id,
		o.creation_user as creation_user,
		a.owner_id,
		wta.party_id,
		o.creation_date,
		im_name_from_user_id(o.creation_user) as owner_name,
		acs_object__name(o.object_id) as object_name,
		im_biz_object__get_type_id(o.object_id) as type_id,
		im_biz_object__get_status_id(o.object_id) as status_id,
		tr.transition_name,
		t.holding_user,
		t.task_id,
				to_char(a.start_date, :date_format) as start_date_pretty,
				to_char(a.end_date, :date_format) as end_date_pretty,
				a.description
	from
		acs_objects o,
		wf_cases ca left outer join im_user_absences a on a.absence_id = ca.object_id,
		wf_transitions tr,
		wf_tasks t,
		wf_task_assignments wta
	where
		wta.task_id = t.task_id
		and (wta.party_id in ([template::util::tcl_to_sql_list $supervisor_ids]) or o.creation_user in ([template::util::tcl_to_sql_list $supervisor_ids]) or wta.party_id in (select group_id from group_member_map where member_id in ([template::util::tcl_to_sql_list $supervisor_ids])))
		and o.object_id = ca.object_id
		and ca.case_id = t.case_id
		and t.state in ('active','enabled', 'started')
		and t.transition_key = tr.transition_key
		and t.workflow_key = tr.workflow_key
		and (t.workflow_key in (select distinct aux_string1 from im_categories where category_type = 'Intranet Absence Type' and aux_string1 is not null) or
			 t.workflow_key = 'vacation_storno_wf')
		and a.absence_status_id not in (16002,16005)"
	if {[llength $object_ids] >0 } {
		append manager_tasks_sql "	and o.object_id not in ([template::util::tcl_to_sql_list $object_ids])"
	}

	# Now append the workflows with an "assign me" label
	db_foreach manager_sql $manager_tasks_sql {
		set status [im_category_from_id $status_id]
		set object_url "[export_vars -base "/intranet-timesheet2/absences/new" {{form_mode display} {absence_id $object_id}}]"
		set owner_url [export_vars -base "/intranet/users/view" {return_url {user_id $owner_id}}]
		set workflow_url [export_vars -base "/intranet-workflow/assign" {task_id {return_url [util_get_current_url]}}]

		# Set the supervisor name
		set supervisor_name [im_name_from_user_id $party_id]
		set next_action_l10n [lang::message::lookup "" intranet-workflow.Assign_yourself_instead "Assign to yourself instead of %supervisor_name%"]
		set deny_url ""
		set approve_url ""

		set row_html "<tr$bgcolor([expr $ctr % 2])>\n"
		foreach column_var $column_vars {
			append row_html "\t<td valign=top>"
			set cmd "append row_html $column_var"
			eval "$cmd"
			append row_html "</td>\n"
		}
		append row_html "</tr>\n"
		append table_body_html $row_html
		incr ctr
	}
}


# Show a reasonable message when there are no result rows:
if { [empty_string_p $table_body_html] } {
    set table_body_html "
	<tr><td colspan=$colspan><ul><li><b> 
	[lang::message::lookup "" intranet-core.lt_There_are_currently_n "There are currently no entries matching the selected criteria"]
	</b></ul></td></tr>"
}

# ---------------------------------------------------------------
# Return results

set admin_action_options ""
if {$user_is_admin_p} {
    set admin_action_options "<option value=\"nuke\">[lang::message::lookup "" intranet-workflow.Nuke_Object "Nuke Object (Admin only)"]</option>"
}

set table_action_html "
	<tr class=rowplain>
	<td colspan=99 class=rowplain align=right>
	    <select name=\"operation\">
	    <option value=\"delete_membership\">[lang::message::lookup "" intranet-workflow.Remove_From_Inbox "Remove from Inbox"]</option>
	    $admin_action_options
	    </select>
	    <input type=submit name=submit value='[lang::message::lookup "" intranet-workflow.Submit "Submit"]'>
	</td>
	</tr>
    "
set enable_bulk_action_p [parameter::get_from_package_key -package_key "intranet-workflow" -parameter "EnableWorkflowInboxBulkActionsP" -default 0]
if {!$enable_bulk_action_p} { set table_action_html "" }

set return_url [ad_conn url]?[ad_conn query]
