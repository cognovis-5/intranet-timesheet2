ad_library {

    Initialization for intranet-timesheet2 module
    
    @author Frank Bergmann (frank.bergmann@project-open.com)
    @creation-date 16 November, 2006
    @cvs-id $Id: intranet-timesheet2-init.tcl,v 1.2 2011/11/21 12:15:31 cvs Exp $

}

# Initialize the search "semaphore" to 0.
# There should be only one thread indexing files at a time...
nsv_set intranet_timesheet2 timesheet_synchronizer_p 0

# Check for imports of external im_hours entries every every X minutes
ad_schedule_proc -thread t [parameter::get_from_package_key -package_key intranet-timesheet2 -parameter SyncHoursInterval -default 59 ] im_timesheet2_sync_timesheet_costs

# Schedule the reminders
set remind_employees_p [db_string select_parameter {
    SELECT attr_value FROM apm_parameter_values WHERE parameter_id = (
        SELECT parameter_id FROM apm_parameters WHERE package_key = 'intranet-timesheet2' AND parameter_name = 'RemindEmployeesToLogHoursP'
	);
} -default 0]

if {$remind_employees_p} {
    ad_schedule_proc -thread t -schedule_proc ns_schedule_weekly [list 1 7 0] im_timesheet_remind_employees
}


# Callbacks 
ad_proc -public -callback absence_on_change {
    {-absence_id:required}
    {-absence_type_id:required}
    {-user_id:required}
    {-start_date:required}
    {-end_date:required}
    {-duration_days:required}
    {-transaction_type:required}
} {
    Callback to be executed after an absence has been created
} -

ad_proc -public -callback im_timesheet_hours_new_redirect {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
    {-project_id ""}
    {-julian_date ""}
    {-gregorian_date ""}
    {-show_week_p ""}
    {-user_id_from_search ""}
    {-return_url ""}
} {
    This is mainly a callback to redirect from the original new.tcl page to somewhere else

    @param topic_id ID of the forum topic
    @param topic_type_id ID of type of topic
} -



ad_proc -public -callback im_timesheet_report_filter {
    {-form_id:required}
} {
    This callback is executed after we generated the filter ad_form

    This allows you to extend in the uplevel the form with any additional filters you might want to add.

    @param form_id ID of the form to which we want to append filter elements
} -

ad_proc -public -callback im_timesheet_report_before_render {
    {-view_name:required}
    {-view_type:required}
    {-sql:required}
    {-table_header ""}
    {-variable_set ""}
} {
    This callback is executed before /intranet-reporting/timesheet-customer-project is rendered / the sql command actually executed.

    The callback implementation needs to run ad_script_abort in the uplevel, so you don't execute the SQL statement and try to render the component.

    @param view_name view_name used to render the columns.
    @param view_type The view_type. This can be anything, empty string usually means you want to render the component
    @param sql The SQL string which im_timesheet_task_list_component prepares
    @param table_header Name of the table in the spreadsheet (e.g. in Excel).
    @param variable_set A set of variables to pass through
} -


ad_proc -public -callback im_user_absence_on_submit {
    -form_id:required
    -object_id:required
} {
    This callback allows for additional validations using error_field and error_message upvar variables

    @param object_id ID of the $object_type
} -

ad_proc -public -callback im_user_absence_new_button_pressed {
    {-button_pressed:required}
} {
    This callback is executed after we checked the pressed buttons but before the normal delete / cancel check is executed. 
    
    This allows you to add additional activities based on the actions defined e.g. in the im_user_absence_new_actions. As it is called before delete / cancel you can
    have more actions defined.

} - 


ad_proc -public -callback im_user_absence_new_actions {
} {
    This callback is executed after we build the actions for the new absence form
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_info_actions {
} {
    This callback is executed after we build the actions for the absence info page
    
    This allows you to extend in the uplevel the form with any additional actions you might want to add.

} - 

ad_proc -public -callback im_user_absence_perm_check {
    {-absence_id:required}
} {
    This callback is executed first time we determine that we have an absence_id
    
    This allows you to add additional permission checks, especially against ID guessing.

} - 

ad_proc -public -callback im_trace_column_change {
    {-user_id:required}
    {-object_id:required}
    {-table:required}
    {-column_name:required}
    {-pretty_name:required}
    {-old_value:required}
    {-new_value:required}
} {
    Callback to be executed after a change in a record
} -


ad_proc -callback im_trace_table_change {
    {-object_id:required}
    {-table:required}
    {-message:required}
} {
    Callback to be executed after a set of changes in a table record
} -
