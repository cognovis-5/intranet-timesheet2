# /packages/intranet-timesheet2/www/absences/index.tcl
#
# Copyright (C) 1998-2004 various parties
# The code is based on ArsDigita ACS 3.4
#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# ---------------------------------------------------------------
# 1. Page Contract
# ---------------------------------------------------------------

ad_page_contract {
    Shows all absences. Filters for type, who and when

    @param absence_type_id	if specified, limits view to absences of this type
    @param user_selection	if specified, limits view to absences to mine or all
    @param timescale		if specified, limits view to absences of this time slice
    @param order_by		Specifies order for the table

    @author mbryzek@arsdigita.com
    @author Frank Bergmann (frank.bergmann@project-open.com)
    @author Klaus Hofeditz (klaus.hofeditz@project-open.com)
    @author Alwin Egger (alwin.egger@gmx.net)
    @author Marc Fleischer (marc.fleischer@leinhaeuser-solutions.de)

} {
    { status_id:integer "[im_user_absence_status_requested_or_active]" }
    { start_idx:integer 0 }
    { order_by "User" }
    { how_many "" }
    { absence_type_id:integer "-1" }
    { user_selection "mine" }
    { timescale "future" }
    { view_name "absence_list_home" }
    { start_date "" }
    { end_date "" }
    { user_id_from_search "" }
    { user_department_id:integer ""}
    { project_id ""}
}

# ---------------------------------------------------------------
# 2. Defaults & Security
# ---------------------------------------------------------------

set user_id [auth::require_login]
set admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
set current_user_id $user_id
set subsite_id [ad_conn subsite_id]
set add_absences_for_group_p [im_permission $user_id "add_absences_for_group"]
set add_absences_all_p [im_permission $user_id "add_absences_all"]
set view_absences_all_p [expr [im_permission $user_id "view_absences_all"] || $add_absences_all_p]
set add_absences_direct_reports_p [im_permission $user_id "add_absences_direct_reports"]
set view_absences_direct_reports_p [expr {[im_permission $user_id "view_absences_direct_reports"] || $add_absences_direct_reports_p}]
set add_absences_p [im_permission $user_id "add_absences"]
set org_absence_type_id $absence_type_id
set show_context_help_p 1
set name_order [parameter::get -package_id [apm_package_id_from_key intranet-core] -parameter "NameOrder" -default 1]
set today [db_string today "select now()::date"]

# Backward compatability
set filter_status_id $status_id
set timescale_date $start_date

if {$how_many eq "" || $how_many < 1} {
    set how_many [im_parameter -package_id [im_package_core_id] NumberResultsPerPage  "" 50]
}

set all_user_options [im_user_options -include_empty_p 0 -group_name "Employees"]
set direct_reports_options [im_user_direct_reports_options -user_id $current_user_id]
set direct_report_ids [im_user_direct_reports_ids -user_id $current_user_id]

if {"" != $user_id_from_search} { set user_selection $user_id_from_search }

if {![im_permission $user_id "view_absences"] && !$view_absences_all_p && !$view_absences_direct_reports_p} { 
    ad_return_complaint 1 "You don't have permissions to see absences"
    ad_script_abort
}

# Support if we pass a project_id in
if {"" != $project_id} {
    set user_selection $project_id
}

# Custom redirect? You should change all links to this
# page to the new URL, but sometimes you miss links...
set redirect_package_url [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "AbsenceRedirectPackageUrl" -default ""]
if {"" != $redirect_package_url} {
    ad_returnredirect "$redirect_package_url/index"
}

# ---------------------------------------------------------------
# Logic for user_selection
# ---------------------------------------------------------------

# Check if the permissions of the user are restricted
if {!$view_absences_all_p} {
    if {$view_absences_direct_reports_p} {
	# The user can see the absences of his direct reports
	switch $user_selection {
	    all { set user_selection "direct_reports" }
	    mine - direct_reports {
		# Do nothing
	    }
	    default {
		if {[string is integer $user_selection]} {
		    # Only allowed to see absences from direct reports
		    if {[lsearch $direct_report_ids $user_selection] < 0} {
			set user_selection "mine"
		    }
		} else {
		    # Some kind of unknown option was selected
		    set user_selection "direct_reports"
		}
	    }
	}
    } else {
	# The user can only see his own absences
	set user_selection "mine"
    }
}

set today [db_string today "select now()::date"]

if {"" != $user_id_from_search} { set user_selection $user_id_from_search }

if {![im_permission $user_id "view_absences"] && !$view_absences_all_p && !$view_absences_direct_reports_p} { 
    ad_return_complaint 1 "You don't have permissions to see absences"
    ad_script_abort
}
# ---------------------------------------------------------------
# 
# ---------------------------------------------------------------

set user_name $user_selection
if {[string is integer $user_selection]} {
    set user_name [im_name_from_user_id $user_selection]
} else {
    set user_name [lang::message::lookup "" intranet-core.$user_selection $user_selection]
}

set page_title "[lang::message::lookup "" intranet-timesheet2.Absences_for_user "Absences for %user_name%"]"
set context [list $page_title]
set context_bar [im_context_bar $page_title]
set page_focus "im_header_form.keywords"
set absences_url [parameter::get -package_id [apm_package_id_from_key intranet-timesheet2] -parameter "AbsenceURL" -default "/intranet-timesheet2/absences"]
set return_url [im_url_with_query]
set user_view_page "/intranet/users/view"
set absence_view_page "$absences_url/new"


# ---------------------------------------------------------------
# Build Drop-down boxes
# ---------------------------------------------------------------

# If you can view all absenses you can view them for the groups as well
if { $view_absences_all_p } {
	set user_selection_options [im_user_timesheet_absences_options -project_id $project_id -user_selection $user_selection -enable_groups_p 1]
} else {
	set user_selection_options [im_user_timesheet_absences_options -project_id $project_id -user_selection $user_selection]	
}

# Removed as this is part of the new user_selection
# set user_department_options [im_cost_center_options \
#			    -include_empty 1 \
#			    -include_empty_name [lang::message::lookup "" intranet-timesheet2.All "All"] \
#			    -department_only_p 1 \
#	   ]
#	{user_department_id:text(select),optional {label "[_ intranet-core.Department]"} { options $user_department_options}}


# ---------- / setting filter 'User selection' ------------- # 

set timescale_type_list [im_absence_component__timescale_types]

if { ![exists_and_not_null absence_type_id] } {
    # Default type is "all" == -1 - select the id once and memoize it
    set absence_type_id "-1"
}

set end_idx [expr {$start_idx + $how_many - 1}]
set date_format "YYYY-MM-DD"
set date_time_format "YYYY-MM-DD HH24:MI"

# ---------------------------------------------------------------
# 3. Define Table Columns
# ---------------------------------------------------------------

# ---------------------------------------------------------------
# 4. Define Filter Categories
# ---------------------------------------------------------------

# absences_types
set absences_types [im_memoize_list select_absences_types "select absence_type_id, absence_type from im_user_absence_types order by lower(absence_type)"]
set absences_types [linsert $absences_types 0 [lang::message::lookup "" intranet-timesheet2.All "All"]]
set absences_types [linsert $absences_types 0 -1]
set absence_type_list [list]
foreach { value text } $absences_types {
    # Visible Check on the category
    if {![im_category_visible_p -category_id $value]} {continue}
    regsub -all " " $text "_" category_key
    set text [lang::message::lookup "" intranet-core.$category_key $text]
    lappend absence_type_list [list $text $value]
}

# ---------------------------------------------------------------
# 5. Generate SQL Query
# ---------------------------------------------------------------

# Removed as we use the components to display the content (especially the cube)

# ---------------------------------------------------------------
# 6. Format the Filter
# ---------------------------------------------------------------

set form_id "absence_filter"
set object_type "im_absence"
set action_url "/intranet-timesheet2/absences/"
set form_mode "edit"
set l10n_all [lang::message::lookup "" intranet-core.All "All"] 

ad_form \
    -name $form_id \
    -action $action_url \
    -mode $form_mode \
    -actions [list [list [lang::message::lookup {} intranet-timesheet2.Edit Edit] edit]] \
    -method GET \
    -export {start_idx order_by how_many view_name}\
    -form {
	{absence_type_id:text(select),optional {label "[_ intranet-timesheet2.Absence_Type]"} {options $absence_type_list }}
	{user_selection:text(select),optional {label "[_ intranet-timesheet2.Show_Users]"} {options $user_selection_options }}
	{timescale:text(select),optional {label "[_ intranet-timesheet2.Timescale]"} {options $timescale_type_list }}
	{start_date:text(text) \
		{label "[_ intranet-timesheet2.Start_Date]"} \
		{html {size 10}} {value "$start_date"} \
		{after_html {<input type="button" style="height:23px; width:23px; background: url('/resources/acs-templating/calendar.gif');" onclick ="return showCalendar('start_date', 'y-m-d');" >}} \
	}
	{end_date:text(text) \
		{label "[_ intranet-timesheet2.End_Date]"} \ 
		{html {size 10}} {value "$end_date"} \
		{after_html {<input type="button" style="height:23px; width:23px; background: url('/resources/acs-templating/calendar.gif');" onclick ="return showCalendar('end_date', 'y-m-d');" >}} \
	}
        {subscribe:text(inform)
            {label "[_ intranet-core.Subscribe]"}
        }
	{status_id:text(im_category_tree) \
		optional {label #intranet-core.Status#} {value $status_id} \
		{custom {category_type "Intranet Absence Status" translate_p 1 include_empty_name $l10n_all}} \
	}
    } \
    -on_request {
        # Prepare the subscription link
        set salt [db_string salt "select salt from users where user_id = :current_user_id" -default ""]
        set token [ns_sha1 "${user_id}${salt}"]
        regsub -all {http://} [ad_url] {} server_name
        regsub -all {https://} $server_name {} server_name
        set subscribe_url [export_vars -base "webcal://${server_name}/intranet-timesheet2/absences/ics/${user_id}-${token}.ics" -url {user_selection}]
        switch $user_selection {
            mine {
                set calname "[im_name_from_user_id $current_user_id] Absences"
            } 
            direct_reports {
                set calname "Direct Reports"               
            }
            all - employees {
               set calname "Not Allowed"
               set subscribe_url ""
            }
            default {
                set calname "[im_name_from_id $user_selection] Absences"
            }
        }
        
        set holiday_url [export_vars -base "webcal://${server_name}/intranet-timesheet2/absences/ics/${user_id}-${token}.ics" -url {{user_selection "bank_holiday"}}]
        template::element set_value $form_id subscribe "<a href='$subscribe_url'>$calname</a><br />&nbsp;&nbsp;<a href='$holiday_url'>[lang::message::lookup "" intranet-timesheet2.Holidays_and_br_lt "Bridge Days"]
</A>"
    } \
    -on_submit {
        # Prepare the subscription link
        set salt [db_string salt "select salt from users where user_id = :current_user_id" -default ""]
        set token [ns_sha1 "${user_id}${salt}"]
        regsub -all {http://} [ad_url] {} server_name
        regsub -all {https://} $server_name {} server_name
        set subscribe_url [export_vars -base "webcal://${server_name}/intranet-timesheet2/absences/ics/${user_id}-${token}.ics" -url {user_selection}]
        switch $user_selection {
            mine {
                set calname "[im_name_from_user_id $current_user_id] Absences"
            } 
            direct_reports {
                set calname "Direct Reports"               
            }
            all - employees {
               set calname "Not Allowed"
               set subscribe_url ""
            }
            default {
                set calname "[im_name_from_id $user_selection] Absences"
            }
        }
        template::element set_value $form_id subscribe "<a href='$subscribe_url'>$calname</a>"
    }
template::element::set_value $form_id start_date $start_date
template::element::set_value $form_id end_date $end_date
template::element::set_value $form_id timescale $timescale
template::element::set_value $form_id user_selection $user_selection

eval [template::adp_compile -string {<formtemplate style="tiny-plain-po" id="absence_filter"></formtemplate>}]
set filter_html $__adp_output

# ---------------------------------------------------------------
# Create Links from Menus 
# ---------------------------------------------------------------
set for_user_id $current_user_id

if {[string is integer $user_selection]} { 
    # Log for other user "than current user" requires permissions
    # user_selection can be the current_user, a "direct report" or any other user.

    # Permission to log for any user - OK
    if {$add_absences_all_p} {
	set for_user_id $user_selection
    }

    if {!$add_absences_all_p && $add_absences_direct_reports_p} {
	set direct_reports [im_user_direct_reports_ids -user_id $current_user_id]
	if {[lsearch $direct_reports $user_selection] > -1} {
	    set for_user_id $user_selection
	}
    }
}

set admin_html [im_menu_ul_list "timesheet2_absences" [list user_id_from_search $for_user_id return_url $return_url]]

# ----------------------------------------------------------
# Set color scheme 
# ----------------------------------------------------------

append admin_html "[im_absence_cube_legend]"

# ---------------------------------------------------------------
# 7. Format the List Table Header - MOVED TO LIB
# ---------------------------------------------------------------

# ---------------------------------------------------------------
# 8. Format the Result Data - MOVED TO LIB
# ---------------------------------------------------------------

# ---------------------------------------------------------------
# Left Navbar
# ---------------------------------------------------------------


set left_navbar_html "
	    <div class=\"filter-block\">
		<div class=\"filter-title\">
		[lang::message::lookup "" intranet-timesheet2.Filter_Absences "Filter Absences"]
		</div>
		$filter_html
	    </div>
	    <hr/>

	    <div class=\"filter-block\">
		<div class=\"filter-title\">
		[lang::message::lookup "" intranet-timesheet2.Admin_Absences "Admin Absences"]
		</div>
		$admin_html
	    </div>
"

