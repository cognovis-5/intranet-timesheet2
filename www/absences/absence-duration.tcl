# /packages/intranet-timesheet2/www/absences/absence-action.tcl
#
# Copyright (C) 2019 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Calculate the duration of an absence in days or hours,
    including public holidays for the specific user.
    
    @param start_date start of the absence, assuming 9:00
    @param end_date end of the absence, assuming 18:00
    @author frank.bergmann@project-open.com
} {
    start_date
    end_date
    {absence_owner_id ""}
    {absence_id ""}
}

if {$absence_owner_id eq ""} {set absence_owner_id [auth::require_login]}

set work_days [im_absence_calculate_absence_days -start_date "[join [template::util::date get_property linear_date_no_time $start_date] "-"]" -end_date "[join [template::util::date get_property linear_date_no_time $end_date] "-"]" -owner_id $absence_owner_id -ignore_absence_ids $absence_id -absence_id $absence_id]

set work_units_uom [parameter::get_from_package_key -package_key "intranet-timesheet2" -parameter "AbsenceDefaultDurationUnit" -default "days"]
switch $work_units_uom {
    "hours" {
	set work_units [expr 8.0 * $work_days]
	set work_units_uom_l10n [lang::message::lookup "" intranet-core.hours Hours]
    }
    "days" {
	set work_units $work_days
	set work_units_uom_l10n [lang::message::lookup "" intranet-core.days Days]
    }
    default {
	error "Invalid value for parameter AbsenceDefaultDurationUnit"
	set work_units "error"
    }
}

if {[regexp {^(.*)\.0$} $work_units match rounded_work_units]} {
    set work_units $rounded_work_units
}
# ad_return_complaint 1 $work_units


doc_return 200 "text/html" "$work_units $work_units_uom_l10n"
